from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('search',views.search, name='search'),
    path('search/<str:label>/', views.detail, name='detail'),
]