import os
from rdflib import Graph

def query_list_universe(input_search):
    return """SELECT DISTINCT ?input ?label
    
    WHERE {
        
            ?input rdfs:label ?label .
            ?input ?a dbr:NASA
            filter(regex(?label, """+"'.*"+input_search+".*'"+""", "i" )) .
            filter (lang(?label) = 'en') .
        
    }"""

def query_list_local(input_search):
    return """SELECT DISTINCT ?input ?label
    
    WHERE {
        
            ?input rdfs:label ?label .
            filter(regex(?label, """+"'.*"+input_search+".*'"+""", "i" )) .
        
    }"""

def get_local_list(item):
    os.listdir("./static")
    staticfiles = os.path.join(os.getcwd(),"./static")
    rdfs = os.path.join(staticfiles,"rdfs.txt")
    
    g = Graph()
    g.parse(rdfs, format="turtle")
    qres = g.query(query_list_local(item))

    datum = []
    
    for row in qres:
        datum.append((str(row[0]) , str(row[1])))
        
    return datum


def get_list(item):
    return get_global_list(item) + get_local_list(item)

def query_global_item_uri(input_search):
    return """SELECT DISTINCT ?relationLabel ?input ?inputLabel 
    WHERE{
        %s ?relation ?input.
        ?relation rdfs:label ?relationLabel.
        ?input rdfs:label ?inputLabel.
        filter (lang(?relationLabel) = 'en').
        filter (lang(?inputLabel) = 'en').
    }""" % (input_search)

def query_global_item_not_uri(input_search):
    return """SELECT DISTINCT ?relationLabel ?teks 
    WHERE{
        %s ?relation ?teks.
        ?relation rdfs:label ?relationLabel.
        filter (lang(?relationLabel) = 'en').
        filter (!isURI(?teks)).
        filter (lang(?teks) = 'en').
    }""" % (input_search)

def get_global_item(item):
    from SPARQLWrapper import SPARQLWrapper, JSON
    
    datum = []

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query_global_item_uri(item))
    print(query_global_item_uri(item))
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        if result['relationLabel']['value'] != 'Link from a Wikipage to another Wikipage':
            datum.append((result['relationLabel']['value'] , result['input']['value'] , result['inputLabel']['value']))
            
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query_global_item_not_uri(item))
    print(query_global_item_not_uri(item))
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    
    for result in results["results"]["bindings"]:
        datum.append((result['relationLabel']['value'] , None , result['teks']['value']))
    
    return datum

def get_global_list(item):
    from SPARQLWrapper import SPARQLWrapper, JSON
    
    datum = []

    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query_list_universe(item))
    print(query_list_universe(item))
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        datum.append((result['input']['value'] , result['label']['value']))
    
    return datum

def query_local_item_no_uri(label):
    return """SELECT DISTINCT ?relationLabel ?teks 
    WHERE{
        %s ?relation ?teks.
        ?relation rdfs:label ?relationLabel.
        filter (!isURI(?teks)).
    }""" % (label)    

def query_local_item_uri(label):
    #Buat Debug
    return """SELECT DISTINCT ?relation ?relationLabel ?input ?inputLabel 
    WHERE{
        %s ?relation ?input.
        filter (isURI(?input)).
        OPTIONAL {?input rdfs:label ?inputLabel}
        OPTIONAL {?relation rdfs:label ?relationLabel}
    }""" % (label)

def find_missing_name(label):
    return """SELECT ?namaLabel
    WHERE{
        %s rdfs:label ?namaLabel.
        filter (lang(?namaLabel) = 'en').
    }""" % (provide_prefix(label))

def provide_prefix(label):
    prefix = "/".join(label.split("/")[:-1])
    suffix = label.split("/")[-1]
    mapping = {
        'http://dbpedia.org/ontology' : 'dbo',
        'http://dbpedia.org/resource' : 'dbr',
        'http://semantic-web-normal.herokuapp.com/home/search' : 'ex',
        'localhost:8000/home/search' : 'ex'
    }
    return '%s:%s' % (mapping[prefix],suffix)


def get_local_item(item):
    from SPARQLWrapper import SPARQLWrapper, JSON
    
    os.listdir("./static")
    staticfiles = os.path.join(os.getcwd(),"./static")
    rdfs = os.path.join(staticfiles,"rdfs.txt")
    
    g = Graph()
    g.parse(rdfs, format="turtle")
    qres = g.query(query_local_item_no_uri(item))

    datum = []
    
    for row in qres:
        datum.append((str(row[0]) , None , str(row[1])))
        
    qres = g.query(query_local_item_uri(item))
    
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")

    for row in qres:
        items = [str(row[I]) for I in range(4)]
        if items[1] == 'None':
            sparql.setQuery(find_missing_name(items[0]))
            sparql.setReturnFormat(JSON)
            results = sparql.query().convert()

            for result in results["results"]["bindings"]:
                items[1] = result['namaLabel']['value']
                
        if items[3] == 'None':
            sparql.setQuery(find_missing_name(items[2]))
            sparql.setReturnFormat(JSON)
            results = sparql.query().convert()

            for result in results["results"]["bindings"]:
                items[3] = result['namaLabel']['value'] 

        datum.append((str(items[1]) , str(items[2]) , str(items[3])))
        
    return datum
