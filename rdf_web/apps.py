from django.apps import AppConfig


class RdfWebConfig(AppConfig):
    name = 'rdf_web'
