from django.shortcuts import render
from rdflib import Graph
from django.contrib.staticfiles.storage import staticfiles_storage

from .utils import get_list , get_local_item , get_global_item, provide_prefix

def index(request):
    return render(request, 'searches/search_box.html')

def search(request):
    input_search = request.POST.get('search')
    
    qres = get_list(input_search)

    form = {"result":[]}
    for row in qres:
        try:
            homebaseurl = "http://semantic-web-normal.herokuapp.com/home/search"
            form["result"].append({"obj":provide_prefix(row[0]),"label":str(row[1]),"url":homebaseurl+"/"+provide_prefix(row[0])})
        except KeyError:
            continue
    print(form)
    return render(request, 'searches/search_result.html', form)

def detail(request, label):
    form = {"result":[],"label":label}
    
    if label.startswith("ex:"): # Berati ada di dataset kita
        result = get_local_item(label)
        for row in result:
            form["result"].append({"obj":row[0],"label":row[2]})
    else:
        result = get_global_item(label)
        print("#",result)
        for row in result:
            print(row)
            form["result"].append({"obj":row[0] ,"label":row[2]})
    
    print(form)
    return render(request, 'searches/search_item.html', form)

